<?php

namespace Example\Pokemon\Model\Pokemon;
use Example\Pokemon\Interface\Data\PokemonInterface;
use Example\Pokemon\Interface\PokemonRepositoryInterface;
use Example\Pokemon\Model\Pokemon;

class Repository implements PokemonRepositoryInterface {

    const API_URL = 'https://pokeapi.co/api/v2/pokemon/%s';
    /**
     * @inheritDoc
     */
    public function getPokemonById(int $id) : PokemonInterface {
        $url = sprintf(self::API_URL, $id);
        if ($pokomonResponse = $this->curlCall($url)) {
            $pokemon = new Pokemon();
            $pokemon->setName(ucwords($pokomonResponse['name']));
            $pokemon->setImage($pokomonResponse['sprites']['front_default']);
            

            return $pokemon;
        }
        throw new \Exception(sprintf('No Pokémon found with id: %s', $id));
    }

    private function curlCall($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result, true);
    }
}