<?php

namespace Example\Pokemon\Model;
use Example\Pokemon\Interface\Data\PokemonInterface;

class Pokemon implements PokemonInterface
{
    const NAME = 'name';
    const IMAGE = 'image';

    /**
     * Model data
     * @var array
     */
    private array $data;

    public function __construct() {
        $this->data = [];
    }

    /**
     * @inheritDoc
     */
    public function getName() : string {
        return $this->data[self::NAME];
    }
    
    /**
     * @inheritDoc
     */
    public function getImage() : string {
        return $this->data[self::IMAGE];
    }


    /**
     * Summary of setName
     * @param string $name
     * @return \Example\Pokemon\Model\Pokemon
     */
    public function setName(string $name) : Pokemon {
        $this->data[self::NAME] = $name;
        return $this;
    }
    
   
    /**
     * Summary of setName
     * @param string $image
     * @return \Example\Pokemon\Model\Pokemon
     */
    public function setImage(string $image) : Pokemon {
        $this->data[self::IMAGE] = $image;
        return $this;
    }

    /**
     * Get Pokemon data
     * @return array
     */
    public function getData() {
        return $this->data;
    }
}
