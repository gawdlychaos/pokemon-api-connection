<?php

namespace Example\Pokemon\Interface;

use Example\Pokemon\Interface\Data\PokemonInterface;

interface  PokemonRepositoryInterface {

    public function getPokemonById(int $id) : PokemonInterface;
}