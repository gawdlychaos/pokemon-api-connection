<?php

namespace Example\Pokemon\Interface\Data;

interface PokemonInterface
{

    /**
     * Retrieve name
     * @return string
     */
    public function getName() : string;

    /**
     * Retrieve image path
     * @return string
     */
    public function getImage() : string;

    

}
